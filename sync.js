/**
 * Created by david on 20/10/2015.
 */
var Client = require('node-rest-client').Client;

var args = {
    path: {}
}

var options_auth = {user: "admin", password: "admin"};

process.argv.forEach(function (val, index, array) {
    var match = /(\w+)=([^ ]+)/i.exec(val);
    if (match) {
        var arg = match[1],
            value = match[2]

        if (arg == "jira")
            args.path.jira = value
        else if (arg == "source")
            args.path.source = value
        else if (arg == "dest")
            args.path.dest = value
        else if (arg == "user")
            options_auth.user = value
        else if (arg == "password")
            options_auth.password = value
    }
});

if (args.path.jira == undefined) {
    console.log("Usage: node sync.js jira=<JIRA-URL> source=<source-project-key> dest=<dest-project-key> user=<user> password=<password>");
    return;
}

// direct way

client = new Client(options_auth);

args.data = {
    project: args.path.dest
}
args.headers = {"Content-Type": "application/json"}

client.get("${jira}/rest/api/2/project/${source}/versions", args,
    function (sourceVersions, response) {
        if (response.statusCode != 200) {
            console.log("Could not get source versions")
            console.log(response.statusCode + ": " + response.statusMessage)
            return
        }

        //now get existing dest versions
        client.get("${jira}/rest/api/2/project/${dest}/versions", args,
            function (existingVersions, response) {
                if (response.statusCode != 200) {
                    console.log("Could not get dest versions")
                    console.log(response.statusCode + ": " + response.statusMessage)
                    return
                }

                doit();

                function doit() {
                    var sourceVersion = sourceVersions.shift();
                    if (!sourceVersion)
                        return;
                    var existingVersionId = undefined

                    args.data.name = sourceVersion.name
                    args.data.archived = sourceVersion.archived
                    args.data.released = sourceVersion.released
                    args.data.releaseDate = sourceVersion.releaseDate

                    //do we already have the version?
                    existingVersions.forEach(function (existingVers) {
                        if (sourceVersion.name == existingVers.name)
                            existingVersionId = existingVers.id
                    })

                    if (!existingVersionId) {
                        //this is a new version, create it
                        console.log("Creating version:")
                        console.log(args.data)
                        client.post("${jira}/rest/api/2/version", args,
                            function (createdVersion, response) {
                                //now update the newly created version with the released flag if it is set (to work around a JIRA bug)
                                if (response.statusCode == 201 && sourceVersion.released && !createdVersion.released) {
                                    console.log("Setting released flag of version " + sourceVersion.name);
                                    args.path.versionid = createdVersion.id
                                    client.put("${jira}/rest/api/2/version/${versionid}", args, function (data, response) {
                                        if (response.statusCode != 200) {
                                            console.log("Could not update dest version")
                                            console.log(response.statusCode + ": " + response.statusMessage)
                                        }
                                        doit();
                                    });
                                } else
                                    doit();
                            })
                    } else {
                        //this version already exists, update it
                        args.path.versionid = existingVersionId
                        client.put("${jira}/rest/api/2/version/${versionid}", args, function (data, response) {
                            if (response.statusCode != 200) {
                                console.log("Could not update dest version " + sourceVersion.name);
                                console.log(response.statusCode + ": " + response.statusMessage)
                            } else
                                console.log("Updated version " + sourceVersion.name + " (id=" + existingVersionId + ")")
                            doit();
                        });
                    }
                }
            })

    });
