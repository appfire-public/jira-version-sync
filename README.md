# README #

### What is this repository for? ###

This is a simple NodeJS script that will copy/update JIRA versions from one project to another. This is useful if you use JIRA Service Desk to support a product whose development you track with JIRA Software, since you now cannot use the same project for both uses anymore.

### How do I get set up? ###

* Install NodeJS
* clone the repository
* go into the workspace and run "npm install"
* run the file with the following parameters:

```
#!
node sync jira=<your-JIRA-URL> source=<source-project-key> dest=<dest-project-key> user=<JIRA-user> password=<JIRA-password>
```


### Known limitations ###

Right now, when a new version is copied over from the source project to the destination project and that version is "released", you will get an error. To fix it, simply run the tool again.